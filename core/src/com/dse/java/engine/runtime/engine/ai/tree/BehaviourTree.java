package engine.runtime.engine.ai.tree;

import engine.runtime.core.uobject.UObject;

/**
 * @author Samuel Huh
 * Began : 03/10/2018
 *
 * Represents an 'utility-based' Behaviour Tree.
 * Essentially, each 'node' has a utility value
 * assigned where the value that is higher is
 * always considered more favorable than the one
 * considered higher.
 *
 * Essentially, the path (a.k.a. sequence of connected nodes until
 * the end of the tree is reached) represents a sequence of commands
 * to be taken by the owner of this tree.
 */
public class BehaviourTree extends UObject{
    private UObject owner; // owner of the Behaviour Tree

}
