package engine.runtime.engine.ai.tree;

import engine.runtime.core.uobject.UObject;

/**
 * @author Samuel Huh
 * Represents the base class for all the
 * nodes in the utility behaviour tree
 *
 * Contains 'actions'
 */
public class UBTNode extends UObject {
    
}
