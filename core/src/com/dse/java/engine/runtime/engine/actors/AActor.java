package engine.runtime.engine.actors;

import engine.runtime.core.tags.ESendMessageOptions;
import engine.runtime.core.uobject.UAgent;
import engine.runtime.core.uobject.components.UComponent;
import engine.runtime.engine.scene.UScene;
import engine.runtime.engine.scene.UWorld;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AActor extends UAgent {
    protected List<AActor> parentList;
    protected List<AActor> childrenList;
    // embedded scene
    protected UScene scene;
    // actor tag for finding / evaluation in the scene
    protected String tag;

    public AActor() {
        super();
        parentList    = new ArrayList<>();
        childrenList  = new ArrayList<>();
        tag           = null;
        scene         = null;
    }

    public AActor(String name) {
        this();
        this.name = name;
    }
    public AActor(String name, Boolean bActiveSelf) {
        this(name);
        this.active      = bActiveSelf;
    }

    /** Constructs the Actor in the defined 'scene' at the defined 'time'
     *
     * For more information, read the 'Actor Lifecycle' in the documentation
     *
     *@param scene the scene (a.k.a. 'level') this Actor will be built in
     *@param owner (optional) the owner of this actor
     */
    public void spawn(UScene scene, AActor owner) {
        spawn(scene, 0.f, owner, null);
    }

    /** Constructs the Actor in the defined 'scene' at the defined 'time'
     *
     * For more information, read the 'Actor Lifecycle' in the documentation
     *
     *@param scene the scene (a.k.a. 'level') this Actor will be built in
     *@param time the time, in seconds, before the spawning happens (DEFAULT : 0)
     *@param owner (optional) the owner of this actor
     */
    public void spawn(UScene scene, Float time, AActor owner, String tag) {
        sleep(time);
        if (owner != null && !parentList.contains(owner))
            parentList.add(owner);
        this.scene = scene;
        this.tag   = tag;
        // this.scene.add(name, this);
        postSpawnInitialize();
        onConstruction();
        preInitializeComponents();
        initializeComponents();
        postInitializeComponents();
        init();
    }

    /**
     * Called during the first phase of loading the
     * components.
     * i.e. calling the components' constructors
     */
    protected void preInitializeComponents() {

    }

    /**
     * Calls the initialization function of the components
     */
    protected void initializeComponents() {

    }

    /**
     * Calls the post initialization function of the components
     */
    protected void postInitializeComponents() {

    }

    /**
     * Initializes the object
     */
    @Override
    protected void init() {

    }

    /**
     * Called after spawn
     */
    protected void postSpawnInitialize() {

    }

    /**
     * Called after spawning. Constructs the body / data of the actor
     * when placed in the scene
     *
     */
    protected void onConstruction() {

    }

    /**
     * Loads the first aspects of the Object
     * from the disk (save file).
     *
     *@param props the save file where the world information is stored
     */
    @Override
    public void load(Properties props) {
        super.load(props);
        preInitializeComponents();
        initializeComponents();
        postInitializeComponents();
        init();
    }
    @Override
    protected void postLoad() {
        super.postLoad();
    }

    public void destroy() {
        beginDestroy();
        endDestroy();
    }

    protected void beginDestroy() {

    }

    protected void endDestroy() {

    }

    /**
     * Adds the component with the defined type
     * and defined name
     *
     *@param classTag the class of the component
     */
    public UComponent addComponent(Class<?> classTag) {
        return addComponent(classTag, null);
    }

    /**
     * Adds the component with the defined type
     * and defined name
     *
     *@param classTag the class of the component
     *@param componentName the name of the component
     */
    public UComponent addComponent(Class<?> classTag, String componentName) {
        UComponent component = getComponent(classTag);
        if (componentExist(classTag)) return component;
        if (componentName == null) componentName = classTag.getName();
        component = new UComponent(componentName, getActorTag(), this);
        componentList.add(component);
        return component;
    }

    public Boolean componentExist(Class<?> classTag) {
        return getComponent(classTag) != null;
    }

    /**
     * Calls the method on every children of this actor
     * @param methodName name of the method; used to identify the method
     * @param params (Optional) parameters of the method
     * @param sendMessageOptions (Optional) the specs of the message
     */
    public void broadcastMessage(String methodName, Object params, ESendMessageOptions sendMessageOptions) {
        if (AActor.sendMessage(this, methodName, params)) return;

        for (AActor actor : childrenList) {
            if (actor.isActive() &&
                AActor.sendMessage(actor,methodName, params))
                return;
        }
    }

    public Boolean compareTag(String tag) {
        return getActorTag().equals(tag);
    }

    public String getActorTag() {
        return tag;
    }

    public UComponent getComponent(Class<?> classTag) {
        return getComponent(classTag, false);
    }

    public UComponent getComponent(Class<?> classTag, Boolean includeInactive) {
        if (!includeInactive && !active) return null;
        for (UComponent component : componentList) {
            if (component.getClass().equals(classTag))
                return component;
        }
        return null;
    }

    public UComponent getComponentRecursive(Class<?> classTag, Boolean includeInactive) {
        UComponent component = getComponent(classTag, includeInactive);
        if (componentExist(classTag)) return component;
        for (AActor child : childrenList) {
            component = child.getComponentRecursive(classTag, includeInactive);
            if (component != null) return component;
        }
        return component;
    }

    public UComponent getComponentInParent(Class<?> classTag) {
        UComponent component = getComponent(classTag);
        if (component != null) return component;
        for (AActor parent : parentList) {
            component = parent.getComponentInParent(classTag);
            if (component != null) return component;
        }
        return component;
    }

    public List<UComponent> getComponents(Class<?> classTag) {
        List<UComponent> components = new ArrayList<>();
        for (UComponent component : componentList) {
            if (component.getClass().equals(classTag))
                components.add(component);
        }
        return components;
    }

    public List<UComponent> getComponentsRecursive(Class<?> classTag) {
        List<UComponent> components = new ArrayList<>();
        for (AActor child : getChildrenList()) {
            components.addAll(child.getComponentsRecursive(classTag));
        }
        return components;
    }

    public List<UComponent> getComponentsInParent(Class<?> classTag) {
        List<UComponent> components = new ArrayList<>();
        for (AActor parent : getParentList()) {
            components.addAll(parent.getComponentsInParent(classTag));
        }
        return components;
    }

    public List<UComponent> getComponents() {
        return componentList;
    }

    public List<AActor> getChildrenList() {
        return childrenList;
    }

    public List<AActor> getParentList() {
        return parentList;
    }

    public UScene getScene() {
        return scene;
    }

    public Boolean isActive() {
        return active;
    }


    /**
     * Finds the actor in the world via the 'actorName.'
     * Returns the first matching actor.
     *
     * @param actorName the name of the desired Actor
     *
     * WARNING : EXPENSIVE OPERATION!
     */
    public static AActor find(String actorName) {
        for (UScene scene : UWorld.getInstance().getScenes()) {
            final AActor actor = scene.findActorByName(actorName);
            if (actor != null) return actor;
        }
        return null;
    }

    /**
     * Finds the actor in the world via the 'actorTag.'
     * Returns the first matching actor.
     *
     * @param actorTag the Tag to find the desired Actor
     */
    public static AActor findByTag(String actorTag) {
        for (UScene scene : UWorld.getInstance().getScenes()) {
            final AActor actor = scene.findActorByTag(actorTag);
            if (actor != null) return actor;
        }
        return null;
    }

    /**
     * Finds all the actors with the matching tag in the World.
     * Returns an List of matching actors.
     *
     * @param actorTag the tag to find the desired actors
     *
     * WARNING : Very expensive operation
     */
    public static List<AActor> findActorsByTag(String actorTag) {
        List<AActor> actors = new ArrayList<>();
        for (UScene scene : UWorld.getInstance().getScenes()) {
            List<AActor> validActors = scene.findActors(actorTag);
            if (!validActors.isEmpty()) actors.addAll(validActors);
        }
        return actors;
    }

}
