package engine.runtime.engine.scene;

/*
 * @author Samuel Huh
 * Represents a section of the UWorld where UObjects are stored
 * and maintained.
 *
 * All UObjects are created / stored in a UScene. There can be multiple
 * instances of the same class in different scenes.
 *
 */

import engine.runtime.core.uobject.UObject;
import engine.runtime.engine.actors.AActor;

import java.util.*;

public class UScene extends UObject {

    // The actors that occupy the scene. These actors
    // include NPCs, bullets, props, etc.
    private Map<String, AActor> inhabitants = new Hashtable<>();
    private EEnvironment EEnvironment = new EEnvironment();


    public UScene() {
        super();
    }

    public UScene(String name) {
        super(name);
    }


    public void destroy() {
        for (String key : inhabitants.keySet()) {
            inhabitants.get(key).destroy();
            inhabitants.remove(key);
        }
        assert(inhabitants.size() == 0);
    }

    public void load() {

    }

    /**
     * Adds the actor into the Scene.
     * The key is the actor's name / hash id
     * by default
     * @param actor actor to add into the scene
     */
    public void add(AActor actor) {
        add(actor.getName(), actor);
    }

    /**
     * Adds the key-actor pair into the Scene
     *
     * If the pair already exists, will replace the
     * value associated with the key with specified
     * actor.
     *
     * @param key the hash key of the actor
     * @param actor actor to add
     */
    public void add(String key, AActor actor) {
        if (key == null || actor == null) return;
        if (inhabitants.containsKey(key)) {
            inhabitants.remove(key);
        }
        inhabitants.put(key, actor);
    }


    public Boolean containsKey(String key) {
        return inhabitants.containsKey(key);
    }

    /**
     * Returns true if the scene contains the key-value pair,
     * as specified. Both the key and the value has to exist and
     * match.
     *
     * Returns false, otherwise.
     * NOTE : Non-Recursive search
     *
     * @param key the key of the object
     * @param actor the object to find in the scene
     */
    public Boolean contains(String key, AActor actor) {
        if (key == null) return false;
        return find(key).equals(actor);
    }

    /**
     * Check if the scene has the proposed
     * actor.
     *
     * @param actor actor to find in the scene.
     */
    public Boolean contains(AActor actor) {
        return inhabitants.containsValue(actor);
    }

    /**
     * Recursive check to see if key is contained in the
     * Scene
     *
     * NOTE : Very expensive operation!
     *
     * @param key the key to check to see if item exists in the Scene
     */
    public Boolean containsKeyRecursive(String key) {
        if (containsKey(key)) return true;
        for (AActor actor : inhabitants.values()) {
            try {
                if (actor.buildTable().contains(key))
                    return true;
            } catch(IllegalAccessException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return false;
    }

    /**
     * Checks to see if the key-value exists in the scene
     * recursively
     *
     * NOTE : Very expensive operation
     *
     * @param key the key of the object
     * @param object the object to find
     */
    public Boolean containsRecursive(String key, UObject object) {
        if (contains(key, (AActor)(object))) return true;
        for (AActor actor : inhabitants.values()) {
            try {
                final Map<String, Object> map = actor.buildTable();
                if ( map.containsKey(key) && map.get(key).equals(object)) {
                    return true;
                }
            } catch(IllegalAccessException ex) {
               System.out.println(ex.getMessage());
            }
        }
        return false;
    }

    /**
     * Check if defined non-Primitive variable exists
     * in the scene. Recursive operation i.e. goes through
     * each baseObject to see if the variable exists!
     *
     * WARNING : Very expensive operation!
     *
     * @param object the Non-Primitive variable to check if
     *               exists in the scene
     */
    public Boolean containsRecursive(Object object) {
        if (contains((AActor)(object))) return true;
        for (AActor actor : inhabitants.values()) {
            try {
                for (Object baseObjVariable : actor.buildTable().values()) {
                    if (baseObjVariable.equals(object))
                        return true;
                }
            } catch (IllegalAccessException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return false;
    }

    /**
     * Removes the object, as specified by the key, from
     * the scene.
     *
     * @param key the key of the object to remove
     * @return the actor that was removed. Null, otherwise.
     */
    public AActor remove(String key) {
        return inhabitants.remove(key);
    }

    /**
     * Removes the actor from the scene. The key-actor must
     * be found in the scene before the object is removed.
     *
     * @param key the key of the object to remove
     * @param actor the actor to remove
     * @return the actor that was removed. Null, otherwise.
     */
    public AActor remove(String key, AActor actor) {
       if (inhabitants.remove(key, actor))
           return actor;
       return null;
    }


    /**
     * Finds the actor, using the defined 'key'
     * Default finding method. Will exhaust
     * 'findActorbyName' and 'findActorByTag'
     * method.
     *
     * @param key the key of the object to find
     * @return the found actor
     */
    public AActor find(String key) {
        AActor actor = inhabitants.get(key);
        if (actor == null) {
            actor = findActorByName(key);
            if (actor == null)
                actor = findActorByTag(key);
        }
        return actor;
    }

    /**
     * Finds the actor by name.
     *
     * @param name the name of the actor
     * @return the found actor. Null, otherwise.
     */
    public AActor findActorByName(String name) {
        return inhabitants.get(name);
    }

    /**
     * Finds the actor by tag
     *
     * @param tag the tag of the actor.
     * @return the found actor. Null, otherwise
     */
    public AActor findActorByTag(String tag) {
        for (AActor actor : inhabitants.values()) {
            if (actor.compareTag(tag)) return actor;
        }
        return null;
    }

    /**
     * Returns an List of actors with the specified
     * tag.
     *
     * @param tag the tag of the desired actor
     */
    public List<AActor> findActors(String tag) {
        List<AActor> actors = new ArrayList<>();
        for (AActor actor : inhabitants.values()) {
            if (actor.getActorTag().equalsIgnoreCase(tag))
                actors.add(actor);
        }
        return actors;
    }

}
