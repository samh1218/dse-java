package engine.runtime.engine.scene;

import engine.runtime.core.uobject.UObject;
import engine.runtime.engine.actors.AActor;

import java.util.*;

/**
 * The UWorld is a singleton class where
 * all the game world's scenes are stored.
 * Game environments and settings are also
 * stored in this class.
 */

public class UWorld extends UObject{
    private static UWorld world = new UWorld("UWorld");
    private static Map<String, UScene> scenes = new Hashtable<>();

    private UWorld() {
        super();
    }
    private UWorld(String name) {
        super(name);
    }

    public static UWorld getInstance() {
        return world;
    }

    public void addScene(UScene scene) {
        if (scene == null) return;
        if (scenes.containsKey(scene.toString())) return;
        scenes.put(scene.toString(), scene);
    }

   public Collection<UScene> getScenes() {
        return scenes.values();
    }

    /**
     * Retrieves the specified scene
     * from the Game World
     * @param key the scene to retrieve
     * @return the desired scene if exists; NULL, otherwise
     */
    public UScene getScene(String key) {
        if (key == null) return null;
        return scenes.get(key);
    }

    /**
     * Retrieves the specified actor
     * from the specified scene
     * @param sceneKey the key of the scene where the actor is stored
     * @param actorKey the key of the actor
     * @return the desired actor if exists; null, otherwise
     */
    public AActor getActor(String sceneKey, String actorKey) {
        if (sceneKey == null || actorKey == null)
            return null;
        return scenes.get(sceneKey).find(actorKey);
    }



    /**
     * Destroys the specified scene, if scene exists
     *
     * @param sceneKey the key of the scene to destroy
     */
    public void destroyScene(String sceneKey) {
        scenes.get(sceneKey).destroy();
        scenes.remove(sceneKey);
    }

    /**
     * Destroys the World.
     *
     */
    @Override
    public void destroy() {
        for (String key : scenes.keySet()) {
            destroyScene(key);
        }
        world = null;
    }
}
