package engine.runtime.engine.actors.characters;

import engine.runtime.engine.actors.AActor;

public class ACharacter extends AActor {
    public class PersonalityTraits {
        PersonalityTraits() {}
        public int CHARM             = 5;
        public int COURAGE           = 5;
        public int TENACITY          = 5;
        public int EMPATHY           = 5;
        public int LOYALTY           = 5;
        public int AGGRESSION        = 5;
        public int CURIOSITY         = 5;
        public int IMAGINATION       = 5;
        public int DECISIVENESS      = 5;
        public int PATIENCE          = 5;
        public int SELF_PRESERVATION = 5;
        public int CRUELTY           = 5;
        public int HUMILITY          = 5;
        public int MEEKNESS          = 5;
        public int COORDINATION      = 5;
        public int VIVACITY          = 5;
        public int CANDOR            = 5;
        public int BULK_APPRECIATION = 5;
        public int HUMOUR            = 5;
    }
    public PersonalityTraits traits = new PersonalityTraits();

    /**
     * Decide whether to receive the trigger
     * or not. Scores the strength of the trigger
     * and determines whether or not the Character
     * should respond to that trigger.
     *
     * @return true if the Character should respond;
     *         false, otherwise.
     */
    public Boolean receiveTrigger() {
        return false;
    }

    public ACharacter() {
        super();
    }

    public ACharacter(String name) {
        super(name);
    }

    /**
     * Begins the Dialog process for this Character
     */
    public void speak() {

    }


}
