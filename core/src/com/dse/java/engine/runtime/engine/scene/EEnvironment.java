package engine.runtime.engine.scene;

public class EEnvironment {
    public enum BIOME {
        TUNDRA,
        TAIGA,
        TEMPERATE,
        DESERT,
        GRASSLAND,
        TROPICS
    }

    /**
     * Dependencies :
     * - BIOME (Enum) : affects the duration and
     * strength of the season
     */
    public enum SEASON {
        SPRING,
        SUMMER,
        FALL,
        WINTER
    }

    public enum WEATHER {
        CLEAR,
        SNOW,
        RAIN,
        FOG,
        SNOW_STORM,
        RAIN_STORM
    }

    /**
     * The temperature, in Celsius, of the
     * EEnvironment. Naturally affected by
     * the season and biome of the environment.
     */
    private int temperature;

    private BIOME biome;

    private WEATHER weather;

    private SEASON season;

    public EEnvironment() {
        temperature = 16;
        biome = BIOME.TEMPERATE;
        season = SEASON.SPRING;
        weather = WEATHER.CLEAR;
    }

    public int getTemperature() {
        return temperature;
    }

    public BIOME getBiome() {
        return biome;
    }

    public WEATHER getWeather() {
        return weather;
    }

    public SEASON getSeason() {
        return season;
    }
}
