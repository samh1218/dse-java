package engine.runtime.launch;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import game.narrative.UPage;

public class Launcher extends Game {
    private SpriteBatch batch;
    private BitmapFont  font;
    private UPage       page;
    private String      gameName = "DSE";

    private static Launcher launcher = new Launcher();

    public static Launcher getInstance() {
        return launcher;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameName() {
        return gameName;
    }

    @Override
    public void create() {
        batch = new SpriteBatch();
        font  = new BitmapFont();
        font.setColor(Color.WHITE);
        page = new UPage(gameName);
        page.init();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        page.run(batch, font);
        batch.end();
    }

    @Override
    public void dispose() {

    }

    public static void main(String[] args) {
        final LwjglApplicationConfiguration configuration =
                new LwjglApplicationConfiguration();
        final LwjglApplication application =
                new LwjglApplication(Launcher.getInstance(), configuration);
        Input input = application.getInput();
        while (!input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            if (input.isKeyJustPressed(Input.Keys.LEFT))
                System.out.println("Previous");
            else if (input.isKeyJustPressed(Input.Keys.RIGHT))
                System.out.println("Next");
            input = application.getInput();
        }
        application.exit();
        System.out.println("Exit System ... ");
    }
}
