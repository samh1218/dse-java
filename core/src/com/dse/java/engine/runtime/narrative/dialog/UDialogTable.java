package engine.runtime.narrative.dialog;

import engine.runtime.core.uobject.UObject;
import engine.runtime.core.utilities.UConfigReader;

import java.io.File;
import java.io.IOException;

public class UDialogTable extends UObject {
    public UDialogTable() {
        super();
    }
    public UDialogTable(String name) {
        super(name);
    }

    public void init() {
        try {
            final String configPath =
                    System.getProperty("user.dir") + UConfigReader.getPropertyFile("path.properties").getProperty("path.dialog");
            final File dialogDir = new File(configPath);
            final File[] files = dialogDir.listFiles();
            for (File file : files) {
                if (file.getName().endsWith(".xml")) {
                    // Get the dialog units and add to hashtable

                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
