package engine.runtime.narrative.dialog;

import engine.runtime.core.uobject.UObject;
import engine.runtime.engine.actors.characters.ACharacter;
import engine.runtime.engine.scene.UScene;

import java.util.Hashtable;
import java.util.Map;

public class UQueryMap extends UObject {
    private Map<String, Object> queryMap;
    private ACharacter owner;
    public UQueryMap() {
        super();
        owner = null;
    }

    public UQueryMap(ACharacter owner) {
        this(owner, new Hashtable<>());
    }

    public UQueryMap(ACharacter owner, Map<String, Object> queryMap) {
        this();
        this.queryMap = queryMap;
        this.owner = owner;
    }
    public UQueryMap(String name, ACharacter owner) {
        super(name);
        this.owner = owner;
    }
    public UQueryMap(String name, ACharacter owner, Map<String,Object> queryMap) {
        this(name, owner);
        this.queryMap = queryMap;
    }

    /**
     * Adds the criterion (key-value pair)
     * @param key the key of the criterion
     * @param object the value, when compared to what exist,
     *               must be equal to this object in order
     *               for the criterion to pass.
     */
    public void add(String key, Object object) {
        queryMap.put(key, object);
    }

    /**
     * Retrieves the object from the QueryMap,
     * using the object's key.
     *
     * @param key the key of the queryMap
     */
    public Object get(String key) {
        return queryMap.get(key);
    }

    /**
     * Retrieves the owner of this instance.
     * @return the owner of this instance.
     */
    public ACharacter getOwner() {
        return owner;
    }

    /**
     * Retrieves the scene of the owner.
     * @return the scene where the owner is located.
     */
    public UScene getScene() {
        if (owner != null)
            return owner.getScene();
        return null;
    }

    /**
     * Removes the element at the specified key
     * and returns that deleted element.
     *
     * @param key the key of the object to remove.
     * @return the deleted element, if exists. Null, otherwise.
     */
    public Object remove(String key) {
        //return UWorld.getInstance().remove(key);
        return null;
    }

    /**
     * Returns the class of the key of the object.
     *
     * @param key the key of the object in question
     * @return the class of the object in question if
     *         object exists
     */
    public Class<?> getClass(String key) {
        //return UWorld.getInstance().getClass(key);
        return null;
    }

    public static void main(String[] args) {
        // Setting up the variables needed for testing
//        UWorld.getInstance().addData("onNewGame", true);
//        UWorld.getInstance().addScene(new UScene("demoScene"));
//        UWorld.getInstance().addData("narrator", new ACharacter("narrator"));

//        UQueryMap queryMap = new UQueryMap((ACharacter)(UWorld.getInstance().get("narrator")));
    }
}
