package engine.runtime.narrative.dialog;

import engine.runtime.core.uobject.UObject;
import engine.runtime.engine.actors.AActor;
import engine.runtime.engine.actors.characters.ACharacter;

import java.util.*;

public class UResponse extends UObject {
    public class UResponseOption {
        private String dialogLine;
        private ACharacter triggerActor;
        private Map<String, Object> applyFacts;

        public UResponseOption(String dialogLine) {
            this.dialogLine = dialogLine;
            triggerActor = null;
            applyFacts = new Hashtable<>();
        }
        public UResponseOption(String dialogLine, ACharacter triggerActor) {
            this(dialogLine);
            this.triggerActor = triggerActor;
        }

        public String getDialogLine() {
            return dialogLine;
        }

        public ACharacter getTriggerActor() {
            return triggerActor;
        }

        public void setApplyFacts(Map<String, Object> applyFacts) {
            this.applyFacts = applyFacts;
        }

        public Map<String, Object> getApplyFacts() {
            return applyFacts;
        }

        public void addApplyFact(String key, Object value) {
            applyFacts.put(key, value);
        }

        public void runApplyFacts() {
//            for (String key : applyFacts.keySet())
//                UWorld.getInstance().update(key, applyFacts.get(key));
        }
    }
    private List<UResponseOption> options;

    private ACharacter speaker;

    private int maxNumOfApplyFacts;

    private UResponse() {
        super();
        speaker = null;
        options = new ArrayList<>();
        maxNumOfApplyFacts = 0;
    }

    public UResponse(final ACharacter speaker) {
        this();
        this.speaker = speaker;
    }

    public void run(UQueryMap queryMap) {
        final UResponseOption response = selectHighestScoredOption(queryMap);
        // DEBUG MESSAGE
        System.out.println(response.getDialogLine());
        //--------------------
        response.runApplyFacts();
    }

    protected UResponseOption selectHighestScoredOption(UQueryMap queryMap) {
        Map<Integer, Float> idxAndScore = new Hashtable<>();
        UResponseOption highestScoredOption = options.get(0);
        float highestScore = score(highestScoredOption, queryMap);

        int idx = 0;
        for (UResponseOption option : options) {
            final float optionScore = score(option, queryMap);
            if (optionScore > highestScore) {
                highestScore = optionScore;
                highestScoredOption = option;
            } else if (optionScore == highestScore) {
                idxAndScore.put(idx, optionScore);
            }
            idx++;
        }
        List<UResponseOption> bestOptions = new ArrayList<>();
        for (int key : idxAndScore.keySet()) {
            if (idxAndScore.get(key) == highestScore) {
                bestOptions.add(options.get(key));
            }
        }

        final Random randomizer = new Random();
        final int bestOptionIdx = randomizer.nextInt(bestOptions.size() - 1);

        return bestOptions.get(bestOptionIdx);
    }

    public Boolean speakerIsAbleToSpeak() {
        return speaker != null && speaker.isActive();
    }

    /**
     * Scores the responseOption
     * Score is in the Range from 0 to 1,
     * with 1 being the highest
     * /--------------------/
     * As of 09/04/2017 : No complex computation for scoring
     * Currently, any complex mathematics only complicates but do
     * not notably improve the dialog selection process
     *
     * Scoring Method :
     * 1. if option has a triggerActor, has a 60% point
     * 2. # of 'facts' in applyFacts per response : higher
     *    makes up a higher point for the remaining 20%
     *
     * /---------------------/
     */
    public Float score(UResponseOption option, UQueryMap queryMap) {
        float optionScore = 0.2f;

        if (option.getTriggerActor() != null) {
            final Object target = queryMap.get(option.getTriggerActor().toString());
            if (target != null) {
                AActor actor = null;
                if (target instanceof AActor)
                    actor = (AActor)(target);
                if (actor != null && actor.isActive())
                    optionScore += 0.6f;
                else return optionScore;
            }
            if (!option.getApplyFacts().isEmpty()) {
                float ratio = (float)(option.getApplyFacts().size()) / (float)(maxNumOfApplyFacts);
                ratio *= 0.2f;
                optionScore += ratio;
            }
        }
        return optionScore;
    }

    public void add(UResponseOption option) {
        if (option != null)
            options.add(option);
        if (option.getApplyFacts().size() > maxNumOfApplyFacts)
            maxNumOfApplyFacts = option.getApplyFacts().size();
    }

    public void remove(UResponseOption option) {
        options.remove(option);
        findMaxNumOfApplyFacts();
    }

    protected void findMaxNumOfApplyFacts() {
        maxNumOfApplyFacts = 0;
        for (UResponseOption option : options) {
            final int numOfApplyFacts = option.getApplyFacts().size();
            if (numOfApplyFacts > maxNumOfApplyFacts)
                maxNumOfApplyFacts = numOfApplyFacts;
        }
    }
}
