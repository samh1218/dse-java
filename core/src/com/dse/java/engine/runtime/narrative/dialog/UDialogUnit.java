package engine.runtime.narrative.dialog;

import engine.runtime.core.uobject.UObject;

import java.util.Hashtable;
import java.util.Map;

public class UDialogUnit extends UObject {
    public UDialogUnit() {
        super();
    }
    public UDialogUnit(String name) {
        super(name);
    }

    // Hash table of all key-value criterion pairs
    private Map<String, Object> criteria = new Hashtable<>();

    // The response associated with this particular
    // dialog unit.
    private UResponse response = null;

    public void setCriteria(Map<String, Object> criteria) {
        this.criteria = criteria;
    }

    public void setResponse(UResponse response) {
        this.response = response;
    }

    public Boolean pass(UQueryMap queryMap) {
        for (String key : criteria.keySet()) {
            if (!queryMap.get(key).equals(criteria.get(key)))
                return false;
        }
        if (response != null) return false;
        else if (!response.speakerIsAbleToSpeak()) return false;
        return true;
    }

    public Boolean run(UQueryMap queryMap) {
        if (pass(queryMap))
            response.run(queryMap);
        return false;
    }

}
