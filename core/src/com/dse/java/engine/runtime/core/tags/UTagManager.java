package engine.runtime.core.tags;

import java.util.HashSet;

public class UTagManager {
    private HashSet<String> tags = new HashSet<>();
    private static UTagManager instance = new UTagManager();
    private UTagManager() {}
    public static UTagManager getInstance() {
        return instance;
    }

    /*
     * Adds the tag in the manager
     * if the tag does not already exist
     *
     */
    public void add(String tag) {
        if (tag != null) tags.add(tag);
    }

    /*
     * Returns true if the tag exists in the
     * UTagManager. Returns false, otherwise.
     */
    public Boolean contains(String tag) {
       return tags.contains(tag);
    }

    /*
     * Attempts to remove the tag, if the tag
     * exists in the TagManager.
     *
     * Returns true if successful.
     * False otherwise.
     */
    public Boolean remove(String tag) {
        if (contains(tag)) {
            tags.remove(tag);
            return true;
        }
        return false;
    }

    /*
     * Returns the tags in array format.
     */
    public String[] toArray() {
        String[] tagArray = new String[tags.size()];
        int i = 0;
        for ( String tag : tags) {
            tagArray[i++] = tag;
        }
        return tagArray;
    }
}
