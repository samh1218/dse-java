package engine.runtime.core.containers.graphs;

import engine.runtime.core.containers.graphs.edges.URelationEdge;
import engine.runtime.engine.actors.characters.ACharacter;

public class URelationshipGraph<V extends ACharacter> extends DirectedGraph<V> {
    public static int INVALID_AFFINITY = -200;

    @Override
    public void connect(V vertexOne, V vertexTwo) {
        super.connect(vertexOne, vertexTwo);
    }
    @Override
    protected void connectNodes(V vertexOne, V vertexTwo) {
        verticesTable.get(vertexOne).put(vertexTwo.hashCode(), new URelationEdge<>(vertexOne, vertexTwo));
        numOfEdges++;
    }

    public int getAffinity(V vertexOne, V vertexTwo) {
        if (contains(vertexOne) && contains(vertexTwo)) {
            URelationEdge<V> edge =  (URelationEdge<V>) (verticesTable.get(vertexOne).get(vertexTwo.hashCode()));
            if (edge != null) return edge.getAffinity();
        }
        return INVALID_AFFINITY;
    }

    public void setAffinity(V vertexOne, V vertexTwo, int affinityValue) {
        if (affinityValue < -100 && affinityValue != INVALID_AFFINITY)
            affinityValue = -100;
        if (affinityValue > 100)
            affinityValue = 100;
        if (contains(vertexOne) && contains(vertexTwo)) {
            URelationEdge<V> edge = (URelationEdge<V>) (verticesTable.get(vertexOne).get(vertexTwo.hashCode()));
            if (edge != null) {
                edge.setAffinity(affinityValue);
                verticesTable.get(vertexOne).replace(vertexTwo.hashCode(), edge);
            }
        }
    }

    public static void main(String[] args) {
        // Initial Variables ...
        URelationshipGraph<ACharacter> relationshipGraph =
                new URelationshipGraph<>();
        ACharacter obj1 = new ACharacter();
        ACharacter obj2 = new ACharacter();
        // Connect obj1 and obj2
        relationshipGraph.connect(obj1, obj2);
        // Test #1: if Edge of the Graph is URelationEdge
        assert(relationshipGraph.getAffinity(obj1, obj2) == 0);
        // Test #2: change affinity value between -100 and 100, inclusive.
        relationshipGraph.setAffinity(obj1, obj2, 100);
        assert(relationshipGraph.getAffinity(obj1, obj2) == 100);
        relationshipGraph.setAffinity(obj1, obj2, 0);
        assert(relationshipGraph.getAffinity(obj1, obj2) == 0);
        // Test #3: change affinity value outside of -100 and 100 inclusive range
        relationshipGraph.setAffinity(obj1, obj2, -101);
        assert(relationshipGraph.getAffinity(obj1, obj2) == -100);
        relationshipGraph.setAffinity(obj1, obj2, 101);
        assert(relationshipGraph.getAffinity(obj1, obj2) == 101);
        relationshipGraph.setAffinity(obj1, obj2, -200);
        assert(relationshipGraph.getAffinity(obj1, obj2) ==  -200);
    }
}
