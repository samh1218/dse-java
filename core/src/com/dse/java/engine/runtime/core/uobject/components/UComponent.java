package engine.runtime.core.uobject.components;

import engine.runtime.core.tags.ESendMessageOptions;
import engine.runtime.core.tags.UTagManager;
import engine.runtime.core.uobject.UAgent;
import engine.runtime.core.uobject.UObject;
import engine.runtime.engine.actors.AActor;

import java.util.Hashtable;

/**
 * @author Samuel Huh
 * Base class for components that define reusable behaviour.
 * Attached to actors to compose the actor's main method lists.
 *
 * Based around the Entity-Component-System architecture. Refer to
 * that section in the documentation for further information.
 */
public class UComponent extends UObject {
    protected UAgent owner;
    protected String componentTag;

    public UComponent() {
        super();
        this.owner = null;
        this.componentTag = null;
    }

    public UComponent(String name, AActor owner) {
        this(name, null, owner);
    }

    public UComponent(String name, String componentTag, AActor owner) {
        super();
        this.name = name;
        this.componentTag = componentTag;
        this.owner = owner;
        UTagManager.getInstance().add(this.componentTag);
    }

    @Override
    public Hashtable<String, Object> buildTable() throws IllegalAccessException {
        return super.buildTable();
    }

    public UObject getOwner() { return owner; }

    /**
     * Attempts to invoke the method via reflection if the method
     * with the 'methodName' exists in the owner of the component.
     *
     * Base function for all message-sending functions.
     *
     * @param methodName the name of the method we are attempting to discover
     * @param params the object that may possess a method with the methodName and parameters
     * @param sendMessageOptions the way the message will be send (Optional)
     */
    public void sendMessage(String methodName, Object params, ESendMessageOptions sendMessageOptions) {
        if (owner == null) return;
        owner.sendMessage(methodName, params, sendMessageOptions);
    }

    public Boolean compareTag(String tag) {
        return componentTag.equalsIgnoreCase(tag);
    }

    public UComponent getComponent(Class<?> classType) {
        if (owner instanceof AActor)
            return getComponent(classType, owner);
        return null;
    }

    public UComponent getComponent(Class<?> classType, UAgent owner) {
        if (owner != null && classType != null) {
            for (UComponent component : owner.getComponents()) {
                if (component.getClass().equals(classType))
                    return component;
            }
        }
        return null;
    }
}
