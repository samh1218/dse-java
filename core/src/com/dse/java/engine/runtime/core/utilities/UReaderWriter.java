package engine.runtime.core.utilities;

import java.io.*;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class UReaderWriter {

    protected static UReaderWriter readerWriter = new UReaderWriter();

    public static UReaderWriter getInstance() {
        return readerWriter;
    }

    public List<String> readLines(File file) throws IOException {
        final BufferedReader br = new BufferedReader(new FileReader(file));
        List<String> lines = new LinkedList<>();
        String line;
        while ((line = br.readLine()) != null)
            lines.add(line);
        return lines;
    }

    public List<String> readUniqueLines(File in) throws IOException {
        Set<String> set = new LinkedHashSet<>(readLines(in));
        return new LinkedList<>(set);
    }

    public int lineCount(File inFile) throws IOException {
        return readLines(inFile).size();
    }

    public int uniqueLineCount(File inFile) throws IOException {
        return readUniqueLines(inFile).size();
    }

    public void writeLines(List<String> lines, File outFile, Boolean append) throws IOException {
        final FileWriter writer = new FileWriter(outFile, append);
        if (lines != null) {
            for (String line : lines)
                writer.write(line + "\n");
        }
        writer.flush();
        writer.close();
    }

    public File recreateFile(File file) throws IOException {
        file.delete();
        file.createNewFile();
        return file;
    }

    public void copyFile(File inFile, File outFile) throws IOException {
        List<String> lines = readLines(inFile);
        outFile = recreateFile(outFile);
        writeLines(lines, outFile, true);
    }
}
