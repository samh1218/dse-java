package engine.runtime.core.containers.graphs.edges;

import engine.runtime.engine.actors.characters.ACharacter;

public class URelationEdge<V extends ACharacter> extends GenericEdge<V> {
    /*
     * How close is the relationship between vertexOne and vertexTwo
     */
    protected int affinity;
    public URelationEdge(V vertexOne, V vertexTwo) {
        affinity = 0;
        this.setOrigin(vertexOne);
        this.setDest(vertexTwo);
    }

    public void setAffinity(int affinity) {
        this.affinity = affinity;
    }
    public int getAffinity() { return affinity; }
}
