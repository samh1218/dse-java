package engine.runtime.core.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class UConfigReader {
    public static Properties getPropertyFile(String fileName) throws IOException {
        final String path = System.getProperty("user.dir") + "/engine/src/main/resources/config/";
        final Properties props = new Properties();
        InputStream inputStream = new FileInputStream(path + fileName);
        props.load(inputStream);
        return props;
    }
}
