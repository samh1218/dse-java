package engine.runtime.core.algorithms;

public class AStar {
    private class Node {
        private int x;
        private int y;
        int traversalCost;
        int totalCost;
        Node parent;
        public Node(int heuristic) {
            this.traversalCost = Integer.MAX_VALUE;
            this.totalCost = heuristic;
        }
        public int getTraversalCost() {
            return traversalCost;
        }
        public int getTotalCost() {
            return totalCost;
        }
    }

    /**
     * Returns the cost a.k.a. Manhattan Distance
     * between Node a and Node b
     * @param a the starting Node
     * @param b the end Node
     * @return the cost between Node a and Node b
     */
    private int heuristic(Node a, Node b) {
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
    }

}
