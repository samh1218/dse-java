package engine.runtime.core.uobject;

import engine.runtime.core.tags.ESendMessageOptions;
import engine.runtime.core.uobject.components.UBehaviour;
import engine.runtime.core.uobject.components.UComponent;
import engine.runtime.engine.actors.AActor;

import java.util.List;

/**
 * @author Samuel Huh
 * Began Date : 03/10/2018
 *
 * UObjects that can interact and influence
 * the game world. They do not need to be spawned.
 *
 * As such, agents do not have parents or children; agents
 * are considered independent and self-sustaining, unlike AActors
 * which consider the possibility of parents or children.
 * All AActors are UAgents. All UAgents are not AActors.
 */
public class UAgent extends UObject {
    // list of components attached to this agent.
    protected List<UComponent> componentList;
    // if active, this agent affects the game world
    protected Boolean active;

    public UAgent() {
        super();
        active = true;
    }

    public UAgent(String name) {
        this();
        this.name = name;
    }

    public UAgent(String name, Boolean active, String tag) {
        this(name);
        this.active = active;
    }

    public List<UComponent> getComponents() {
        return componentList;
    }

    public Boolean isActive() {
        return active;
    }


    public void sendMessage(String methodName, Object object, ESendMessageOptions sendMessageOptions) {
        final Boolean successfullySent = AActor.sendMessage(this, methodName, object);
        if (successfullySent && sendMessageOptions.equals(ESendMessageOptions.REQUIRE_RECEIVER))
            throw new IllegalArgumentException("No Method : " + methodName + " found in Actor " + toString());
    }

    /**
     * Attempts to activate the specified methodName in the target actor.
     * The target actor attempts to find the method in its Components; if
     * the actor has a UBehaviour that has the method, will attempt to
     * invoke the method.
     * @param target the actor that may or may not have a method with
     *               the specified methodName
     * @param methodName the name to identify the method
     * @param params (Optional) parameters of the method.
     * @return true if method has been found and activated; false, otherwise.
     */
    public static Boolean sendMessage(UAgent target, String methodName, Object params) {
        if (target == null || !target.isActive())
            return false;
        for (UComponent component : target.getComponents()) {
            if (component instanceof UBehaviour) {
                ((UBehaviour) component).invoke(methodName, params);
            }
        }
        return false;
    }

}
