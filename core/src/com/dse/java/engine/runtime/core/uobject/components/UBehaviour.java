package engine.runtime.core.uobject.components;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class UBehaviour extends UComponent {

    /**
     * Fixed rate (time in seconds, for interval)
     * for fixedUpdate function
     */
    private long fixedUpdateTime;

    /**
     * Number of threads used for updating functions
     */
    private int numOfThreads;

    // Thread pool for dealing with repeating methods
    private ScheduledThreadPoolExecutor repeatMethodThreadPool;

    /**
     * Hashtable storing repeating methods
     * Key : the name of the recurring method
     * ScheduledFuture : the task that requires updating
     */
    Hashtable<String, ScheduledFuture<?>> repeatMethodTable;

    /**
     *
     * @param name the name of the component
     * @param componentTag the tag of the component
     * @param fixedUpdateTime the fixed interval of time for updating this component
     * @param numOfThreads the number of threads used by this component
     */
    public UBehaviour(String name,
                      String componentTag,
                      long fixedUpdateTime,
                      int numOfThreads
                      )
    {
        super();
        this.name            = name;
        this.componentTag    = componentTag;
        this.fixedUpdateTime = fixedUpdateTime;
        this.numOfThreads    = numOfThreads;
    }
    public UBehaviour() {
        super();
        this.name         = this.getClass().getName();
        this.componentTag = null;
        this.owner        = null;
        fixedUpdateTime   = 5;
        numOfThreads      = 5;
    }

    /**
     * Cancels all 'invoke()' calls on this Behaviour.
     * Basically all recurring method are cancelled when
     * this method is called.
     */
    public void cancelInvoke() {
        for (String key : repeatMethodTable.keySet()) {
            cancelInvoke(key);
        }
    }

    /**
     * Same as above but filtered to only those methods
     * with the matching names
     */
    public Boolean cancelInvoke(String methodName) {
        ScheduledFuture methodThread = repeatMethodTable.get(methodName);
        if (methodThread != null && !methodThread.isCancelled()) {
            methodThread.cancel(false);
            return true;
        }
        return false;
    }
    /**
     * Invokes the method 'methodName' in 'time' seconds
     * then repeatedly every 'repeatRate' seconds
     * Only cancelled when owning actor is destroyed
     * or cancelled explicitly by calling the 'cancelInvoke'
     *
     * As of 08/31/2017 : DO NOT USE!!
     */
    public void invokeRepeating(String methodName,
                                Object object,
                                long time,
                                long repeatRate) throws NoSuchMethodException {
        final Method method = this.getClass().getMethod(methodName);
        if (method == null) return;

        final Runnable threadTask = new Runnable() {
            @Override
            public void run() {
                invoke(methodName, object);
            }
        };

        final ScheduledFuture task = repeatMethodThreadPool.scheduleAtFixedRate(
                threadTask, time, repeatRate, TimeUnit.SECONDS
        );
        repeatMethodTable.put(methodName, task);
    }

    public void invokeRepeating(String methodName) throws NoSuchMethodException {
        invokeRepeating(methodName, null, 1, 1);
    }

    public void invoke(String methodName) {
        invoke(methodName, null);
    }
    public void invoke(String methodName, Object object) {
        try {
            final Method method = this.getClass().getMethod(methodName);
            if (method != null) {
                if (object != null)
                    method.invoke(this);
                else
                    method.invoke(this, object);
            }
        }
        catch (IllegalAccessException illegalAccessException) {
            System.out.println(illegalAccessException.getMessage());
        }
        catch (InvocationTargetException invocationException) {
            System.out.println(invocationException.getMessage());
        }
        catch (NoSuchMethodException methodException) {
            System.out.println(methodException.getMessage());
        }
    }

    //----------//
    // MESSAGES //
    //----------//

    /**
     * Called when the Behaviour instance is being loaded.
     * 'Awake' is used to initialize any variables or states
     * before the game starts. Method is called only once during
     * the lifetime of this object. Awake() is called after all
     * objects are initialized so you can safely speak to other
     * objects or query
     *
     * Use Awake() to set up references between scripts and use
     * 'Start' to pass any information back and forth
     */
    public void awake() {
        // Add the fixedUpdate(), Update(), and lateUpdate()
        // methods into the recurring methods table
        final String fixedMethodName      = "fixedUpdate";
        final String updateMethodName     = "update";
        final String lateUpdateMethodName = "lateUpdate";
        try {
            invokeRepeating(fixedMethodName, null, 1, fixedUpdateTime);
            invokeRepeating(updateMethodName);
            invokeRepeating(lateUpdateMethodName);
        }
        catch (NoSuchMethodException ex) {
            System.out.println(ex.getMessage());
        }
    }
    /**
     * This function is called every fixed framerate frame for
     * dealing with methods requiring being called at fixed
     * interval
     */
    public void fixedUpdate() {

    }

    public void update() {

    }

    /**
     * Called every frame. Called after all 'Update' functions
     * have been called
     */
    public void lateUpdate() {

    }

    /**
     * Called when player gets or loses focus (ex. Alt-tabbing)
     * Sent to all actors (by the world). Will be run every frame
     * until player resumes
     */
    public void onApplicationFocus(Boolean hasFocus) {
        if (!hasFocus) {
            for (ScheduledFuture task : repeatMethodTable.values()) {
                try {
                    task.wait();
                }
                catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
        else {
            for (ScheduledFuture task : repeatMethodTable.values()) {
                task.notify();
            }
        }
    }
}
