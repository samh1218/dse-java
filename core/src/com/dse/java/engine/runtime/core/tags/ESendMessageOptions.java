package engine.runtime.core.tags;

public enum ESendMessageOptions {
    REQUIRE_RECEIVER            // Receiver is required for sendMessage
    , DONT_REQUIRE_RECEIVER     // No Receiver is required for sendMessage
}
