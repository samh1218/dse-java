package engine.runtime.core.uobject;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.Properties;
import java.util.zip.CRC32;

public class UObject {
    /* Name of the Object. Name is used to find
       the particular UObject in game. */
    protected String name;

    // When true, Object remains when loading a new level
    // Default : false
    protected Boolean bDontDeleteLoadLevel;

    public UObject() {
        name = this.getClass().getName();
        bDontDeleteLoadLevel = false;
    }

    public UObject(String name) {
        this(name, false);
    }

    public UObject(String name, Boolean bDontDeleteLoadLevel) {
        this.name = name;
        this.bDontDeleteLoadLevel = bDontDeleteLoadLevel;
    }

    // begins the loading process for the object
    // function is used when loading the object from disk
    public void load(Properties props) {
        name = props.getProperty("name");
        name = props.getProperty("bDontDeleteLoadLevel");
    }

    protected void postLoad() {

    }

    protected void init() {

    }

    public void destroy() {

    }


    /**
     * Changes the value of the variable
     * with the given value, if found.
     * NOTE: An expensive operation
     * @param varName the name of the variable
     * @param value the name of the value
     * @return true if successfully set; false otherwise.
     */
    public boolean set(String varName, Object value) {
        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.getName().equalsIgnoreCase(varName)) {
                try {
                    field.set(this, value);
                    return true;
                } catch (IllegalAccessException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
        return false;
    }

    /**
     * Constructs a hashtable that stores all variables
     * used for finding data during the dialogue / ai decision-making
     * process
     *
     *
     */
    public Hashtable<String, Object> buildTable() throws IllegalAccessException{
        Hashtable<String, Object> dataTable = new Hashtable<>();
        for (Field obj : this.getClass().getDeclaredFields()) {
            System.out.println(obj.getName());
            System.out.println(obj.get(this).toString());
            dataTable.put(obj.getName(), obj.get(this));
        }
        for (Field obj : this.getClass().getFields()) {
            System.out.println(obj.getName());
            System.out.println(obj.get(this).toString());
            dataTable.put(obj.getName(), obj.get(this));
        }
        return dataTable;
    }

    public void setName(String name) { this.name = name; }
    public void setDontDeleteLoadLevel(Boolean bDontDeleteLoadLevel)
    { this.bDontDeleteLoadLevel = bDontDeleteLoadLevel; }
    public String getName() { return this.name; }
    public Boolean getbDontDeleteLoadLevel()
    { return bDontDeleteLoadLevel; }

    @Override
    public int hashCode() {
        final CRC32 crc32 = new CRC32();
        crc32.update(name.getBytes());
        return (int)(crc32.getValue());
    }

    protected void sleep(float time) {
        final long startTime = System.nanoTime();
        float elapsedTime = (float)(System.nanoTime() - startTime) / 1000000000.0f;
        while (elapsedTime < time) {
            elapsedTime = (float)(System.nanoTime() - startTime) / 1000000000.0f;
        }
    }

    // Saves the Object into the Object into the Objects' Properties file
    public void save() throws IllegalAccessException, IOException {
        final Properties props = new Properties();
        final String path = System.getProperty("user.dir"); //+ UConfigReader.getPropertiesFile("paths.properties").getProperty("path.object")
        final Hashtable<String, Object> dataTable = buildTable();
        for (Object obj : dataTable.values()) {
            props.setProperty(name + "." + obj.getClass().getName(), obj.toString());
        }
        props.store(new FileWriter(path, true), name);
    }

    /**
     * Unit testing for UObject
     * @param args params for the unit testing
     */
    public static void main(String[] args) {
        // Construct the UObject
        UObject object = new UObject();
        String prevObjName = object.name;
        object.set("name", "hello");
        assert(object.name.equalsIgnoreCase("hello"));
        assert(!object.name.equalsIgnoreCase(prevObjName));
        System.out.println("Prev Name : " + prevObjName + " " + "New Name : " + object.name);
    }
}
