package engine.runtime.core.containers.graphs;

import engine.runtime.core.uobject.UObject;
import engine.runtime.core.containers.Graph;
import engine.runtime.core.containers.graphs.edges.GenericEdge;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class DirectedGraph<V extends UObject> implements Graph<V>{
    // String -> Hexadecimal
    protected Hashtable<V, Hashtable<Integer, Edge<V>>> verticesTable = new Hashtable<>();

    // # of Connections in the Graph
    protected int numOfEdges = 0;

    // Constructor for the Directed Graph
    public DirectedGraph() {}

    @Override
    public void addVertex(V vertex) {
        if (vertex != null && !contains(vertex))
            verticesTable.put(vertex, new Hashtable<>());
    }

    @Override
    public void removeVertex(V vertex) {
        if (!contains(vertex)) return;
        for (Hashtable<Integer, Edge<V>> edgeTable : verticesTable.values()) {
            if (edgeTable.contains(vertex.hashCode())) {
                edgeTable.remove(vertex.hashCode());
                numOfEdges--;
            }
        }
        numOfEdges -= verticesTable.get(vertex).size();
        verticesTable.get(vertex).clear();
        verticesTable.remove(vertex);
    }

    @Override
    public List<V> neighbors(V vertex) {
        List<V> neighbors = new ArrayList<>();
        return neighbors;
    }

    @Override
    public void connect(V vertexOne, V vertexTwo) {
        if (contains(vertexOne) && contains(vertexTwo)) {
            if (!vertexOne.equals(vertexTwo) && !isConnected(vertexOne, vertexTwo)) {
                connectNodes(vertexOne, vertexTwo);
            }
        }
    }

    /*
     * The actual action of connecting.
     */
    protected void connectNodes(V vertexOne, V vertexTwo) {
        verticesTable.get(vertexOne).put(vertexTwo.hashCode(),
                        new GenericEdge<>(vertexOne, vertexTwo));
        numOfEdges++;
    }

    @Override
    public Boolean isConnected(V vertexOne, V vertexTwo) {
        return contains(vertexTwo) &&
                contains(vertexOne)&&
                verticesTable.get(vertexOne).contains(vertexTwo.hashCode());
    }

    @Override
    public Boolean contains(V vertex) {
        return vertex != null && verticesTable.contains(vertex);
    }

    @Override
    public void removePath(V vertexOne, V vertexTwo) {
        if (contains(vertexOne) && contains(vertexTwo)) {
            if (verticesTable.get(vertexOne).contains(vertexTwo.hashCode())) {
                verticesTable.get(vertexOne).remove(vertexTwo.hashCode());
                numOfEdges--;
            }
        }
    }

    @Override
    public int degree(V vertex) {
        if (contains(vertex))
            return verticesTable.get(vertex).size();
        return -1;
    }

    @Override
    public int size() {
        return verticesTable.size();
    }

    @Override
    public int edges() {
        return numOfEdges;
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }

    public static void main(String[] args) {
        final UObject obj  = new UObject();
        final UObject obj2 = new UObject("Hello World");
        final DirectedGraph<UObject> directedGraph = new DirectedGraph<>();
        directedGraph.addVertex(obj);
        // Test #1: Adding an obj into the graph
        assert(directedGraph.size() == 1);
        assert(directedGraph.degree(obj) == 0);
        assert(directedGraph.edges() == 0);
        // Test #2 : Test if null is ignored in adding
        directedGraph.addVertex(null);
        assert(directedGraph.size() == 1);
        assert(directedGraph.degree(obj) == 0);
        assert(directedGraph.edges() == 0);
        assert(directedGraph.contains(obj));
        assert(!directedGraph.contains(null));
        // Test #3 : Test removing
        directedGraph.removeVertex(obj);
        assert(directedGraph.size() == 0);
        assert(directedGraph.degree(obj) < 0);
        assert(directedGraph.edges() == 0);
        assert(!directedGraph.contains(obj));
        // Reset
        directedGraph.addVertex(obj);
        // Test #4 : Add a second object
        directedGraph.addVertex(obj2);
        assert(directedGraph.size() == 2);
        assert(directedGraph.edges() == 0);
        assert(directedGraph.contains(obj));
        assert(directedGraph.contains(obj2));
        // Test #5 : Connect obj -> obj2
        directedGraph.connect(obj, obj2);
        assert(directedGraph.size() == 2);
        assert(directedGraph.edges() == 1);
        assert(directedGraph.degree(obj) == 1);
        assert(directedGraph.degree(obj2) == 0);
        assert(directedGraph.isConnected(obj, obj2));
        assert(!directedGraph.isConnected(obj2, obj));
        // Test #6 : Connect obj2 -> obj
        directedGraph.connect(obj2, obj);
        assert(directedGraph.size() == 2);
        assert(directedGraph.edges() == 2);
        assert(directedGraph.degree(obj) == 1);
        assert(directedGraph.degree(obj2) == 1);
        assert(directedGraph.isConnected(obj, obj2));
        assert(directedGraph.isConnected(obj2, obj));
        // Test #7 : Remove Vertex
        directedGraph.removeVertex(obj);
        assert(directedGraph.size() == 1);
        assert(directedGraph.edges() == 0);
        assert(directedGraph.degree(obj) < 0);
        assert(directedGraph.degree(obj2) == 0);
        assert(!directedGraph.isConnected(obj, obj2));
        assert(!directedGraph.isConnected(obj2, obj));
        // Rest -- add and connect the second element
        directedGraph.addVertex(obj);
        assert(directedGraph.size() == 2);
        // Test #8 : Ignore duplicate element
        directedGraph.addVertex(obj2);
        assert(directedGraph.size() == 2);
        assert(directedGraph.edges() == 0);
        assert(directedGraph.degree(obj) == 0);
        assert(directedGraph.degree(obj2) == 0);
        assert(!directedGraph.isConnected(obj, obj2));
        assert(!directedGraph.isConnected(obj2, obj));
        // Reset connections
        directedGraph.connect(obj, obj2);
        assert(directedGraph.size() == 2);
        assert(directedGraph.edges() == 1);
        assert(directedGraph.degree(obj) == 1);
        assert(directedGraph.degree(obj2) == 0);
        assert(directedGraph.isConnected(obj, obj2));
        assert(!directedGraph.isConnected(obj2, obj));
        // Test #9: Ignore duplicate connection (no cycle on same obj)
        directedGraph.connect(obj, obj);
        assert(directedGraph.size() == 2);
        assert(directedGraph.edges() == 1);
        assert(directedGraph.degree(obj) == 1);
        assert(directedGraph.degree(obj2) == 0);
        assert(directedGraph.isConnected(obj, obj2));
        assert(!directedGraph.isConnected(obj2, obj));
        assert(!directedGraph.isConnected(obj, obj));
        // Test #10: Ignore removing a non-existent connection
        directedGraph.removePath(obj2, obj);
        assert(directedGraph.size() == 2);
        assert(directedGraph.edges() == 1);
        assert(directedGraph.degree(obj) == 1);
        assert(directedGraph.degree(obj2) == 0);
        assert(directedGraph.isConnected(obj, obj2));
        // Test #11: Remove an existing path
        directedGraph.removePath(obj, obj2);
        assert(directedGraph.size() == 2);
        assert(directedGraph.edges() == 0);
        assert(directedGraph.degree(obj) == 0);
        assert(directedGraph.degree(obj2) == 0);
        assert(!directedGraph.isConnected(obj, obj2));
        assert(!directedGraph.isConnected(obj2, obj));
    }
}
