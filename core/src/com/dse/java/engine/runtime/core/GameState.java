package engine.runtime.core;

/**
 * @author Samuel Huh
 *
 * Stores the game state
 * such as when a new game has been triggered,
 * when a save file was loaded, etc.
 */
public class GameState {
    public static boolean onNewGame = false;
    public static boolean onGameStart = false;
    public static boolean onGameEnd = false;
    public static boolean onLoadSave = false;
}
