package engine.runtime.core.uobject.components;

import engine.runtime.core.tags.ESendMessageOptions;
import engine.runtime.engine.actors.AActor;

public class UActorComponent extends UComponent {
    public UComponent getComponentRecursive(Class<?> classTag) {
        UComponent component = getComponent(classTag);
        if (owner instanceof AActor) {
            if (component == null) {
                for (AActor actor : ((AActor)(owner)).getChildrenList()) {
                    component = getComponent(classTag, actor);
                    if (component != null) return component;
                }
            }
        }
        return component;
    }


    /**
     * Calls the method named 'methodName' on every 'Behaviour'
     * in the owner of this component and any of its children
     * All parameters except the 'methodName' are in their
     * default value.
     *
     *@param methodName name to identify the needed method
     *@param params (Optional) parameters of the method
     *@param messageOptions (Optional) specs of the message
     **/
    public void broadcastMessage(String methodName, Object params, ESendMessageOptions messageOptions) {
        if (owner == null) return;
        if (owner instanceof AActor)
            ((AActor)(owner)).broadcastMessage(methodName, params, messageOptions);
    }

    /**
     * Calls the method named 'methodName' on every 'Behaviour'
     * in the owner of this component and any of its children
     * All parameters except the 'methodName' are in their
     * default value.
     *
     *@param methodName the method to find and call
     **/
    public void broadcastMessage(String methodName) {
        if (methodName != null) {
            broadcastMessage(methodName, null, ESendMessageOptions.DONT_REQUIRE_RECEIVER);
        }
    }

    public UComponent getComponentInParent(Class<?> classTag) {
        UComponent component = getComponent(classTag);
        if (owner instanceof AActor) {
            if (component == null) {
                for (AActor actor : ((AActor)(owner)).getParentList()) {
                    component = getComponentFromActor(classTag, actor);
                    if (component != null) return component;
                }
            }
        }
        return component;
    }

    private UComponent getComponentFromActor(Class<?> classTag, AActor actor) {
        for (UComponent component : actor.getComponents()) {
            if (component.getClass().equals(classTag))
                return component;
        }
        return null;
    }
}
