package engine.runtime.core.parsers;

import engine.runtime.core.uobject.UObject;
import engine.runtime.narrative.dialog.UDialogUnit;
import engine.runtime.narrative.dialog.UResponse;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class UDialogParser extends UObject {
    private static UDialogParser dialogParser;
    private UDialogParser() {

    }
    public static UDialogParser getInstance() {
        return dialogParser;
    }

    public UDialogUnit readDialog(File inFile) {
        UDialogUnit dialogUnit = new UDialogUnit();
        try {
            Map<String, Object> criteriaTable = new Hashtable<>();
            final SAXReader reader = new SAXReader();
            final Document document = reader.read(inFile);
            final Element root = document.getRootElement();

            // Read the Criteria
            final Node criteraNode = root.selectSingleNode("/dialog/criteria");
            criteriaTable = readGroup(criteraNode, "criterion");

        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return dialogUnit;
    }

    /**
     * Reads the Group from the XML file
     * @param node the node of the Group
     * @param nodeName the name of the node of the group
     * @return the table that stores the key-value pairs.
     */
    protected Map<String, Object> readGroup(Node node, String nodeName) {
        final List<Node> childrenList = node.selectNodes(nodeName);
        Map<String, Object> table = new Hashtable<>();
        for (Node child : childrenList) {
            final String key = child.selectSingleNode("key").getStringValue();
            final Object value = findCorrectType(key,
                    child.selectSingleNode("value").getStringValue());
            if (value != null)
                table.put(key, value);
        }
        return table;
    }

    public UResponse readResponse(File inFile) {
/*
        try {
            final SAXReader reader = new SAXReader();
            final Document document = reader.read(inFile);
            final Element root = document.getRootElement();
            final String speaker = root.selectSingleNode("/response/speaker").getStringValue();

            final UResponse response = new UResponse((ACharacter)(UWorld.getInstance().get(speaker)));

            final Node optionsNode = root.selectSingleNode("/response/options");
            final List<Node> optionsList = new LinkedList<>();
            for (Object value : optionsNode.selectNodes("option"))
                optionsList.add((Node)(value));
            for (Node child : optionsList) {
                Map<String, Object> applyFactsTable = new Hashtable<>();
                final String line = child.selectSingleNode("line").getStringValue();
                final Node triggerActorNode = child.selectSingleNode("triggerActor");
                ACharacter triggerActor = null;
                if (triggerActorNode != null)
                    triggerActor = (ACharacter)(UWorld.getInstance().
                                    get(triggerActorNode.getStringValue()));
                final Node applyFactsNode = child.selectSingleNode("applyFacts");
                applyFactsTable = readGroup(applyFactsNode, "fact");

                UResponseOption responseOption = new UResponseOption(line, triggerActor);
                responseOption.setApplyFacts(applyFactsTable);
                response.add(responseOption);

                //-----------//
                // TEMPORARY //
                //-----------//
                System.out.println("Response Line : " + line);
                System.out.println(applyFactsTable.toString());
            }
            return response;
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        */
        return null;
    }

    /**
     * Finds the correct type of the value pointed by the
     * specified key, as specified in the XML document.
     *
     * NOTE : Expensive Operation.
     * @param key the specified key of the value
     * @param value the name of the value
     * @return the value with the correct type, in 'Object'
     */
    private Object findCorrectType(String key, String value) {
        if (value == null || key == null) {
            System.out.println("Invalid Key or Value : " + " [key] : " + " [value] : " + value);
            return null;
        }
/*
        final Object currentVal = UWorld.getInstance().get(key);
        if (currentVal != null) {
            if (currentVal instanceof String) {
                return value;
            } else if (currentVal instanceof Byte) {
                return (Byte)(currentVal);
            } else if (currentVal instanceof Integer) {
                return (int)(currentVal);
            } else if (currentVal instanceof Double) {
                return (double)(currentVal);
            } else if (currentVal instanceof Float) {
                return (float)(currentVal);
            } else if (currentVal instanceof Long) {
                return (long)(currentVal);
            } else if (currentVal instanceof Character) {
                return (char)(currentVal);
            } else if (currentVal instanceof Short) {
                return (short)(currentVal);
            } else if (currentVal instanceof Boolean) {
                return (boolean)(currentVal);
            }
        }
*/
        return null;
    }
}
