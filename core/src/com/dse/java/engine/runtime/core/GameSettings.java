package engine.runtime.core;

/**
 * Stores the configuration information
 * of the game. Will port from the config.properties
 * file upon loading the game for the first time.
 */
public class GameSettings {
    public enum ELanguage {
        ENGLISH, KOREAN
    }
    public static ELanguage language = ELanguage.ENGLISH;

}
