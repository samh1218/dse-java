package engine.runtime.core.containers.graphs.edges;

import engine.runtime.core.containers.Graph;
import engine.runtime.core.uobject.UObject;

public class GenericEdge<V extends UObject> implements Graph.Edge<V> {
    private V vertexOne = null;
    private V vertexTwo = null;

    public GenericEdge() {}

    public GenericEdge(V vertexOne, V vertexTwo) {
        this.vertexOne = vertexOne;
        this.vertexTwo = vertexTwo;
    }

    @Override
    public void setOrigin(V vertexOne) {
        this.vertexOne = vertexOne;
    }

    @Override
    public void setDest(V vertexTwo) {
        this.vertexTwo = vertexTwo;
    }

    @Override
    public V getOrigin() {
        return vertexOne;
    }

    @Override
    public V getDest() {
        return vertexTwo;
    }

    @Override
    public void delete() {
        vertexOne = null;
        vertexTwo = null;
    }
}
