package engine.runtime.core.containers;

import engine.runtime.core.uobject.UObject;

import java.util.List;

public interface Graph<T extends UObject> {
    public interface Edge<T> {
        // Sets the value of vertexOne with the one specified
        public void setOrigin(T origin);
        // Sets the value of vertexTwo with the one specified
        public void setDest(T dest);
        // Gets the value of origin vertex to be used
        public T getOrigin();
        // Gets the value of destination vertex to be used
        public T getDest();
        // Deletes this edge
        public void delete();
    }
    // Retrieves # of vertices
    public int size();

    // Retrieves # of edges
    public int edges();


    // Retrieves the list of neighbors of the specified vertex
    public List<T> neighbors(T vertex);

    /**
     * Checks to see if the specified vertex key
     * already exists within the game
     */
    public Boolean contains(T vertex);

    // Adds the vertex to the graph
    public void addVertex(T vertex);

    // Remove the vertex from the graph
    public void removeVertex(T vertex);

    // Adds the undirected edge v-w to this graph.
    public void connect(T vertexOne, T vertexTwo);

    // Returns true if there is a path from vertexOne
    // to vertexTwo
    public Boolean isConnected(T vertexOne, T vertexTwo);

    // Remove the connection between v-w in this graph
    public void removePath(T vertexOne, T vertexTwo);

    // How many neighboring connections for the vertex
    public int degree(T vertex);

    // Returns string representation of the graph
    @Override
    public String toString();
}
