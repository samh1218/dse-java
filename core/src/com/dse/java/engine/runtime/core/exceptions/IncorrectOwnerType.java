package engine.runtime.core.exceptions;

public class IncorrectOwnerType extends Exception {
    public IncorrectOwnerType(String message) {
        super(message);
    }

    public IncorrectOwnerType(String message, Throwable cause) {
        super(message, cause);
    }
}
