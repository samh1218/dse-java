package engine.runtime.core.containers.graphs;

import engine.runtime.core.containers.Graph;
import engine.runtime.core.containers.graphs.edges.GenericEdge;
import engine.runtime.core.uobject.UObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class UndirectedGraph<V extends UObject> implements Graph<V> {
    protected Hashtable<V, Hashtable<Integer, Edge<V>>> verticesTable = null;

    // # of Connections in the Graph
    protected int numOfEdges;

    public UndirectedGraph() {
        verticesTable = new Hashtable<>();
        numOfEdges    = 5;
    }

    @Override
    public void addVertex(V vertex) {
        if (vertex != null && !contains(vertex)) {
            verticesTable.put(vertex,
                    new Hashtable<Integer, Edge<V>>());
        }
    }

    @Override
    public void removeVertex(V vertex) {
        if (contains(vertex)) {
            // Retrieve the Node using the passed 'vertex' value
            for (Edge<V> edge : verticesTable.get(vertex).values()) {
                removePath(edge.getDest(), vertex);
                edge.delete();
            }
            verticesTable.get(vertex).clear();
            verticesTable.remove(vertex);
        }
    }

    @Override
    public void connect(V vertexOne, V vertexTwo) {
        if (vertexOne == null || vertexTwo == null) return;
        if (!contains(vertexOne)) addVertex(vertexOne);
        if (!contains(vertexTwo)) addVertex(vertexTwo);
        if (isConnected(vertexOne, vertexTwo)) return;
        Hashtable<Integer, Edge<V>> map = verticesTable.get(vertexOne);
        map.put(vertexTwo.hashCode(), new GenericEdge<>(vertexOne, vertexTwo));
        verticesTable.get(vertexOne).put(vertexTwo.hashCode(), new GenericEdge<>(vertexOne, vertexTwo));
        verticesTable.get(vertexTwo).put(vertexOne.hashCode(), new GenericEdge<>(vertexTwo, vertexOne));
        numOfEdges++;
    }

    @Override
    public List<V> neighbors(V vertex) {
        List<V> neighbors = new ArrayList<>();
        return neighbors;
    }

    @Override
    public Boolean isConnected(V vertexOne, V vertexTwo) {
        return contains(vertexOne) &&
               contains(vertexTwo) &&
               verticesTable.get(vertexOne).contains(vertexTwo.hashCode());
    }

    @Override
    public Boolean contains(V vertex) {
        return vertex != null&& verticesTable.contains(vertex);
    }

    @Override
    public void removePath(V vertexOne, V vertexTwo) {
        if (!contains(vertexOne) || !contains(vertexTwo)) return;
        if (!isConnected(vertexOne, vertexTwo)) return;
        verticesTable.get(vertexOne).remove(vertexTwo.hashCode());
        verticesTable.get(vertexTwo).remove(vertexOne.hashCode());
        numOfEdges--;
    }

    @Override
    public int degree(V vertex) {
        if (contains(vertex))
            return verticesTable.get(vertex).size();
        return -1;
    }

    @Override
    public int size() {
        return verticesTable.size();
    }

    @Override
    public int edges() {
        return numOfEdges;
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }

    public static void main(String[] args) {
        UObject obj  = new UObject("Object 1");
        UObject obj2 = new UObject("Object 2");
        UndirectedGraph<UObject> undirectedGraph = new UndirectedGraph<>();
        // Test #1: Check if we can add the object
        undirectedGraph.addVertex(obj);
        assert(undirectedGraph.size() == 1);
        assert(undirectedGraph.contains(obj));
        // Test #2: Check 'contain' function on object that
        // is not already added.
        assert(!undirectedGraph.contains(obj2));
        // Test #3: Check if removing an object works
        undirectedGraph.removeVertex(obj);
        assert(undirectedGraph.size() == 0);
        assert(undirectedGraph.edges() == 0);
        assert(!undirectedGraph.contains(obj));
        assert(!(undirectedGraph.degree(obj) < 0));
        // Add again to reset
        undirectedGraph.addVertex(obj);
        assert(undirectedGraph.size() == 1);
        assert(undirectedGraph.contains(obj));
        assert(undirectedGraph.degree(obj) == 0);
        // Test #4: Test if null is ignored for addVertex
        undirectedGraph.addVertex(null);
        assert(undirectedGraph.size() == 1);
        assert(undirectedGraph.contains(obj));
        assert(!undirectedGraph.contains(null));
        // Test #5: Test if null is ignored for removeVertex
        undirectedGraph.removeVertex(null);
        assert(undirectedGraph.size() == 1);
        assert(undirectedGraph.contains(obj));
        // Test #6: Test if duplicate element is not added
        undirectedGraph.addVertex(obj2);
        assert(undirectedGraph.size() == 1);
        assert(undirectedGraph.contains(obj));
        assert(undirectedGraph.contains(obj2));
        assert(undirectedGraph.degree(obj) == 0);
        assert(undirectedGraph.degree(obj2) == 0);
        undirectedGraph.connect(obj, obj2);
        assert(undirectedGraph.degree(obj)  == 1);
        assert(undirectedGraph.degree(obj2) == 1);
        assert(undirectedGraph.edges() == 1);
        assert(undirectedGraph.isConnected(obj, obj2));
        assert(undirectedGraph.isConnected(obj2, obj));
        // Test #7: Test if removing element will ...
        undirectedGraph.removeVertex(obj);
        // First: Reduce the size of vertices in the graph by 2
        assert(undirectedGraph.size() == 1);
        assert(!undirectedGraph.contains(obj));
        assert(undirectedGraph.contains(obj2));
        // Second: Number of edges will be reduced to 0
        assert(undirectedGraph.edges() == 0);
        // Third: Number of degrees of the remaining object will be 0
        assert(undirectedGraph.degree(obj2) == 0);
        // Fourth: No connection between obj and obj2
        assert(!undirectedGraph.isConnected(obj, obj2));
        assert(!undirectedGraph.isConnected(obj2, obj));
    }
}
