package game.narrative;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import engine.runtime.core.uobject.UObject;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

public class UPage extends UObject {
    public final static float MAX_HEIGHT = 470.f;
    public final static float MIN_HEIGHT = 20.f;
    public final static float X_POS = 5.f;
    public final static float MAX_LENGTH = 500.f;
    public final static float MIN_LENGTH = 20.f;

    private List<Line> lines = new ArrayList<>();
    private int currentLineIdx = 0;

    public UPage() { super(); }
    public UPage(String name) { super(name); }

    @Override
    public void init() {
        Gdx.input.setInputProcessor(
                new InputAdapter() {
                    @Override
                    public boolean keyUp(int keycode) {
                        if (keycode == Input.Keys.RIGHT) {
                            if (++currentLineIdx > lines.size())
                                currentLineIdx = lines.size();
                            return true;
                        }
                        else if (keycode == Input.Keys.LEFT) {
                            if (--currentLineIdx < 0) currentLineIdx = 0;
                            return true;
                        }
                        return false;
                    }
                }
        );
    }

    public void run(SpriteBatch batch, BitmapFont font) {
        for (int i = 0; i < currentLineIdx; i++) {
            font.draw(batch, lines.get(i).line, 5, lines.get(i).position,
                      UPage.MAX_LENGTH, -1, true);
        }
    }

    public void add(String line) {
        if (!checkCapacity(line)) {

        }
    }

    public Boolean checkCapacity(String line) {
        return checkCapacity(line, new BitmapFont());
    }

    /**
     * Checks the capacity of the page, if the
     * line is added
     * @param line the line to be added
     * @param font the font used to print out the line
     * @return true if page will be full, when line is added; false otherwise
     */
    protected Boolean checkCapacity(String line, BitmapFont font) {
        int numOfLines = getNumOfLinesNeeded(line, font);
        for (int i = 0; i < lines.size(); i++) {
            numOfLines += getNumOfLinesNeeded(lines.get(i).line, font);
        }
        return (numOfLines * 20) > UPage.MAX_HEIGHT;
    }

    /**
     * Get the number of lines created in the page
     * when displaying a given string.
     * @param text the string that is to be displayed
     * @return the number of lines requried to display
     *         the given text.
     */
    protected int getNumOfLinesNeeded(String text) {
        return getNumOfLinesNeeded(text, new BitmapFont());
    }

    /**
     * Get the number of lines created in the page
     * when displaying a given string.
     * @param text the string that is to be displayed
     * @param font the style of the text
     * @return the number of lines required to display
     * the given text.
     */
    protected int getNumOfLinesNeeded(String text,
                                      BitmapFont font) {
        float width = new GlyphLayout(font, text).width;
        int numOfLines = 0;
        while (width > 0) {
            width -= UPage.MAX_LENGTH;
            numOfLines += 1;
        }
        return numOfLines;
    }

    private class Line {
        private String line;
        private float position;

        public Line(String line, float position) {

        }
        public String getLine() { return line; }
        public float getPosition() { return position; }
        public void setLine(String line) {
            this.line = line;
        }
        public void setPosition(float position) {
            this.position = position;
        }
    }
}
