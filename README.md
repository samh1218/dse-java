# Dynamic Story Engine

Author : Samuel Huh

This is the overarching project for all the other repos and it is very much a work in progess.

The basic premise of this Engine is :
1. To allow easier assembly of dialogues in video games / chat bots by using simple hash tables, utility scoring, and preprocessed text.
2. To create a simple AI that can simulate a workable dialogue without extensive hand-tuning such as the case of Dialogue Trees.

By accomplishing these two goals, DSE should allow for a much more versatile dialogue-ai in video games which can be applied in a commerical application.

## How it Works :

1. First, you have your writers write each dialogue in an XML format, as specified in the wiki.
2. Second, you create a deck of 'Events' which affect parts of the game world.
3. These Events will be shuffled and the AIDirector will select the top 15 events. He will decide to keep/discard any of the events that do not work well together. The remaining events, the AIDirector will then arrange in such a way to give the players an optimal experience.
4. The activation of an event card dictactes how the world is shaped. The dialogueAI will retrieve the world information and score each of the dialogues in its repository. It will then presume to select the highest scored dialogue.

### Current Version : Part I 
a.) Dialogues in XML Format -> DialogueAI to select the correct dialogues via Hashtable -> Creation of Events.
1. Stores dialogues in XML format. The parameters of the dialogues can be tuned so that the simple AI can position the dialogues correct location without the need for very rigid parameters.
2. Simulates a very simple prologue of a demo game.

### What is Missing (WIP):
1. Timeline/Time classes : Without these two classes, we will not be able to simulate DSE in a full-fletched game.
2. Events : currently, events are a work in progress.
3. PlayerCharacter